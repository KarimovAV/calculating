class Calculating:
    def __init__(self, num_one, num_two, operation):
        self.num_one = num_one
        self.num_two = num_two
        self.operation = operation

    def get_value(self):
        if self.operation == '-':
            return self.num_one - self.num_two
        if self.operation == '+':
            return self.num_one + self.num_two
        if self.operation == '*':
            return self.num_one * self.num_two
        if self.operation == '/':
            return self.num_one / self.num_two


a = Calculating(1, 1, '-')
print(a.get_value())
