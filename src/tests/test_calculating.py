import pytest
from src.app.calculator import Calculating


def test_plus_positive():
    c = Calculating(2, 2, '+')
    assert c.get_value() == 4, 'ERROR: Calculating is incorrect'


def test_minus_positive():
    c = Calculating(2, 2, '-')
    assert c.get_value() == 0, 'ERROR: Calculating is incorrect'


def test_multiply_positive():
    c = Calculating(2, 2, '*')
    assert c.get_value() == 4, 'ERROR: Calculating is incorrect'


def test_division_positive():
    c = Calculating(2, 2, '/')
    assert c.get_value() == 1, 'ERROR: Calculating is incorrect'

